<?php 
require_once "db.class.php";
class Server{
     public $db;
     function __construct(){
	   $this->db=new Database('localhost','soap-test','root','');
	 }
/* 
	function for check user and insert user data into users table  
*/
  public function register($firstname,$lastname,$username,$password) {
  
  
  		$parameters = array('firstname'=>$firstname,'lastname'=>$lastname,'username'=>$username,'password'=>md5($password));
		/* validation for users form */
		if($firstname == '')
		{
			return "*Please enter First Name";
		}
		/*if lastname empty */
	    if($lastname == '')
			{
				return "*Please enter  Last Name";
			}
		/*if username empty */	
		if($username == '')
			{
				return "*Please enter User Name";
			}
		/*if password empty */		
		if($password == '')
			{
				return "*Please enter Password";
			}
		if(strlen($password)<6 || strlen($password)>15 )
			{
				return "*Password must be 6 to 15 characters long";
			}
			$condition='username="'.$username.'"';
			 /* Pass table name,fields,condition  */
			$resultArray= $this->db->select('users','username',$condition);
			/* If user already registred */
			if(empty($resultArray))
			{
				 /* insert into user table user data */
				$this->db->insert('users',$parameters);
				return "User successfully registered.";
			}
			else
			{
				return "*Username already exists.";
			}  
  }
  /* 
	function for user login
  */
  function login($username,$password){
  /*  checking for username and password  */
		 if($username == ''){
				return "*Please enter Username";
			}
		 if($password == ''){
				return "*Please enter Password";
			}	
			$password=md5($password);
			/* query for check username and password in user table */
			$condition='username="'.$username.'" and password="'.$password.'"';
			$resultArray= $this->db->select('users','username',$condition);
			/*check for user login or not */
			if(empty($resultArray))
				{
					return "*No such login in the system. Please try again.";
				}
				else
				{
					return "Successfully logged into system.";
			}  
	 
     }
}
/* 
	run  server side function for user
  */
try {
 /*create soap server object */
  $uri='http://localhost/soap-test/server.php';
  $server = new SOAPServer(NULL,array('uri' =>$uri));
  $server->setClass('Server');
  $server->handle();
}
catch (SOAPFault $f) {
 print $f->faultstring;
}

?>
   