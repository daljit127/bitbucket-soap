<?php

class Database
{
    protected $_server;
    protected $_database;
    protected $_user;
    protected $_password;
    protected $_connection;
    
    /**
     * Sets the credentials for database connection
     *
     * @param	string	Server
     * @param	string	Datenbank
     * @param	string	Benutzer
     * @param	string	Passwort
     * @return	void
     */
    public function __construct($server = 'localhost',$database = 'databasename',$user = 'username',$password = 'password')
    {
        $this->_server   = $server;
        $this->_database = $database;
        $this->_user     = $user;
        $this->_password = $password;
    }

    /**
     * Sends a query to database
     *
     * @param	string		Query
     * @param	bool		get ID of the result
     * @return	resource	Result
     */
    protected function _sendQuery($query, $getId = false)
    {
        $this->_connection = mysql_connect($this->_server, $this->_user, $this->_password);

        mysql_select_db($this->_database, $this->_connection);

        mysql_query('SET NAMES \'utf8\'');
        mysql_query('SET CHARACTER SET \'utf8\'');

        $result = mysql_query($query, $this->_connection);
        $tmpId  = mysql_insert_id($this->_connection);

        mysql_close($this->_connection);
        
        if ($getId) {
            return $tmpId;
        }

        return $result;
    }

    /**
     * Performs a SELECT-Query
     *
     * @param	string		Table name
     * @param	string		Fields to select
     * @param	string		WHERE Clause
     * @param	string		ORDER
     * @param	string		LIMIT
     * @param	bool		false = ASC, true = DESC
     * @param	int		Limitbegin
     * @param	string		GroupBy
     * @param	bool		Activate Monitoring
     * @return	resource	Result
     */
    public function select($table,$fields = '*',$where = '1=1',$order = 'id',$limit = '',$desc = false,
$limitBegin = 0,$groupby = null,$monitoring = false
    )
    {
        $query = 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $where;

        if (!empty($groupby)) {
            $query .= ' GROUP BY ' . $groupby;
        }

        if (!empty($order)) {
            $query .= ' ORDER BY ' . $order;

            if ($desc) {
                $query .= ' DESC';
            }
        }

        if (!empty($limit)) {
            $query .= ' LIMIT ' . $limitBegin . ', ' . $limit;
        }

        $result = $this->_sendQuery($query);

        $resultArray = array();

        while ($row = mysql_fetch_assoc($result)) {
            $resultArray[] = $row;
        }

        /**
         * If monitoring is activated, echo the query
         */
        if ($monitoring) {
            echo $query;
        }

        return $resultArray;
    }

    /**
     * Performs an INSERT-Query
     *
     * @param	string	Table
     * @param	array	Data
     * @return	int     Id of inserted data
     */
    public function insert($table, $objects)
    {
        $query = 'INSERT INTO ' . $table . ' ( ' . implode(',', array_keys($objects)) . ' )';
        $query .= ' VALUES(\'' . implode('\',\'', $objects) . '\')';

        $result = $this->_sendQuery($query, true);

        return $result;
    }

}


